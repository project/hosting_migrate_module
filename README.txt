CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

This module allows to launch migrate import through the Aegir interface.

REQUIREMENTS
------------

This module requires the following modules:
 * Hosting (https://www.drupal.org/project/hosting)

This module requires no additional library.

INSTALLATION
------------

 * Install this module like any other drupal 7 module.
 * Enable this module using the hosting features list of Aegir.

MAINTAINERS
-----------

Current maintainers:
 * Florent Torregrosa (Grimreaper) - https://www.drupal.org/user/2388214

This project has been sponsored by:
 * Smile - http://www.smile.fr
