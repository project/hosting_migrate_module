<?php

/**
 * @file
 * The hosting feature definition for hosting_migrate_module.
 */

/**
 * Implements hook_hosting_features().
 */
function hosting_migrate_module_hosting_feature() {
  $features['migrate_module'] = array(
    'title' => t('Migrate support'),
    'description' => t('Allows to launch migrate import through the Aegir interface.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_migrate_module',
    'group' => 'experimental',
  );

  return $features;
}
