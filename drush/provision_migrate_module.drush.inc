<?php

/**
 * @file
 * Provision/Drush hooks for the hosting_migrate_module module.
 *
 * These are the hooks that will be executed by the drush_invoke function.
 */

/**
 * Implements hook_drush_command().
 */
function provision_migrate_module_drush_command() {
  $items['provision-migrate_module'] = array(
    'description' => 'Run migrate import on a site',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Implements the provision-migrate_module command.
 */
function drush_provision_migrate_module() {
  drush_errors_on();
  provision_backend_invoke(d()->name, 'migrate-import', array(), array('all' => TRUE));
  drush_log(dt('All migrations were imported'));
}
